fx_version 'adamant'

game 'gta5'

description 'Testing Images'

version '0.0.1'

ui_page 'page.html'


files {
	'page.html',
	'img/500/*.png',
	'img/1000/*.png',
	-- 'img/1500/*.png',
	-- 'img/2000/*.png',
	-- 'img/2500/*.png',
	-- 'img/3000/*.png',
	-- 'img/3500/*.png',
	-- 'img/3926/*.png'
}
	
client_scripts {
	'c_main.lua'	
}